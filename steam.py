# coding=utf8
"""
steam.py - Show steam application prices/releases/etc

Copyright 2016 ridelore, https://github.com/ridelore/sopel-modules
Copyright 2016 Adam Erdman, https://pandorah.org [*]
[*](Additional formatting & Updated for sopel 6.3.0+)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

from __future__ import unicode_literals
from sopel.module import commands, example, rule
from bs4 import BeautifulSoup
import requests
import re

steam_re = r'(.*:)//(store.steampowered.com)(:[0-9]+)?(.*)'
colorlogo = u'\x02\x0315,01[Steam]\x0F'

def setup(bot):
    regex = re.compile(steam_re)
    if not bot.memory.contains('url_callbacks'):
        bot.memory['url_callbacks'] = tools.sopelMemory()
    bot.memory['url_callbacks'][regex] = steam_url


def get_steam_info(url):
    # we get the soup manually because the steam pages have some odd encoding troubles
    headers = { 'Cookie':'lastagecheckage=1-January-1992; birthtime=694252801' }
    page = requests.get(url, headers=headers)
    soup = BeautifulSoup(page.text, 'lxml', from_encoding="utf-8")

    name = "\x02" + soup.find('div', {'class': 'apphub_AppName'}).text.strip() + "\x0F"
    desc = ": \x1d" + soup.find('div', {'class': 'game_description_snippet'}).text.strip() + "\x0F"
    desc = (desc[:127] + '…\x0F') if len(desc) > 130 else desc

    # the page has a ton of returns and tabs
    rel_date = soup.find('span', {'class': 'date'}).text.strip()
    tags = soup.find('div', {'class': 'glance_tags'}).text.strip().replace(u'Free to Play', '').replace(u'+', '').split()
    genre = " - \x02Genre:\x0F \x1d" + u', '.join(tags[:4]) + "\x0F"
    date = " - \x02Release date:\x0F \x1d" + rel_date.replace(u"Release Date: ", u"") + "\x0F"
    try:
        original_price = soup.find('div', {'class': 'discount_original_price'}).text.strip()
        discount_price = soup.find('div', {'class': 'discount_final_price'}).text.strip()
        discount_pct = re.sub('-', '', soup.find('div', {'class': 'discount_pct'}).text.strip())
#        discount_countdown = re.sub('SPECIAL PROMOTION! ', '', soup.find('p', {'class': 'game_purchase_discount_countdown'}).text.strip())
        price = '%s (\x02%s Off!\x02)' \
                % (discount_price, discount_pct)
    except AttributeError:
        price = soup.find('div', {'class': 'game_purchase_price price'}).text.strip()
    except AttributeError:
        price = '?'
    if not "Free to Play" in price:
        price = "\x02Price:\x02 \x1d" + price + "\x0F"
    price = " - " + price

    return colorlogo + u' {}{}{}{}{}'.format(name, desc, genre, date, price)


@rule(steam_re)
def steam_url(bot, match):
    return bot.say(get_steam_info("http://store.steampowered.com" + match.group(4)))


@commands('steam')
@example('!steam [search] - Search for specified game/trailer/DLC')
def steamsearch(bot, trigger):
    inp = trigger.group(2)
    if not inp:
        return bot.reply(steamsearch.__doc__.strip())
    page = requests.get("http://store.steampowered.com/search/?term=" + inp)
    soup = BeautifulSoup(page.text, 'lxml', from_encoding="utf-8")
    result = soup.find('a', {'class': 'search_result_row'})
    try:
        return bot.say(get_steam_info(result['href']) + " - \x1d" + result['href'].split('?')[0] + "\x0F")
    except:
        return bot.say(colorlogo + u' Failed to find game')
