# coding=utf8
"""
kernelver.py - Kernel Version info module
Copyright © 2016, Adam Erdman https://pandorah.org
Licensed under the Eiffel Forum License 2.
"""

from sopel.config.types import StaticSection, ValidatedAttribute
from sopel.module import commands, interval, rate, rule
import feedparser
import datetime
import urllib


kdist = 'https://www.kernel.org/feeds/kdist.xml'


class KernVerSection(StaticSection):
    channel = ValidatedAttribute('channel')
    topic = ValidatedAttribute('topic')
    cs_command = ValidatedAttribute('cs_string')


def configure(config):
    config.define_section('kernelver', KernVerSection)
    config.kernelver.configure_setting(
        'channel',
        'Enter the channel whose topic we should update (e.g., #pandorah)'
    )
    config.kernelver.configure_setting(
        'cs_string',
        'This module does not require the bot to be OPed to change the topic.'
        ' (Provided it has operator privleges on the channel\'s access list)\n'
        'Enter the appropriate ChanServ command string to set the topic.'
        ' (Message ChanServ \"help\" if you\'re unsure.)\n'
        'Example: SET <channel> TOPIC'
    )
    config.kernelver.configure_setting(
        'topic',
        'Enter the topic string.\n'
        '   You may use the following variables in the title:\n'
        '      $kv  : Latest Stable Linux Kernel Version\n'
        '      $kn  : Latest Stable Linux Kernel Name\n'
        '      $b   : Bold\n'
        '      $u   : Underline\n'
        '      $i   : Italic\n'
        '      $c   : Clear/Reset Formatting\n'
    )


@interval(4000) # 66minutes
def regularly_update_topic(bot):
    update_topic(bot)


@commands('kernver','k')
@rate(10)
def kversions(bot, trigger):
    """Display all the latest Linux kernel releases and their age."""
    separator = u' \u0002\u2059\u0002 '
    kernelversions = []
    for items in feedparser.parse(kdist).entries:
        releasedate = datetime.datetime(*(items.published_parsed)[:7])
        kernelversions.append(items.title.replace('longterm','LTS')
            .replace('st','St').replace('main','Main') +
            u' \u001D(' + timesince(releasedate) + u')\u001D')
    bot.say((separator + separator.join(kernelversions) + separator).strip())
    update_topic(bot)


def update_topic(bot):
    try:
        custom_topic = str(bot.config.kernelver.topic).decode('utf-8')
        cs_command = (bot.config.kernelver.cs_command)
        new_topic = (custom_topic
                .replace("$kv", latest_stable())
                .replace("$kn", kernel_name(latest_stable()))
                .replace("$b", u'\u0002')    # Bold
                .replace("$u", u'\u001F')    # Underline
                .replace("$i", u'\u001D')    # Italic
                .replace("$c", u'\u000F')    # Reset Formatting
                .replace("$H", u'\u000303\u2059')) # Insert Separator (FIX ME!!)
        bot.msg('ChanServ', cs_command + ' ' + new_topic)
    except AttributeError:
        return  # Silently fail if not configured


def parse_krelease(v,r):
    return ((feedparser.parse(kdist).entries[v].title).split(': ')[r])


def timesince(d):
    diff = datetime.datetime.utcnow() - d
    s = diff.seconds
    if diff.days == 1:
        return '1 day'
    elif diff.days > 1:
        return '{} days'.format(diff.days)
    elif s < 60:
        return '{} secs'.format(s)
    elif s < 120:
        return '1 min'
    elif s < 3600:
        return '{:.0f} mins'.format(s/60)
    elif s < 7200:
        return '1 hr'
    return '{:.0f} hrs'.format(s/3600)


def latest_stable():
    """Obtain Latest Stable Kernel Verson."""
    count = 0
    while count < 5: # check the latest 5 kernels for latest 'stable'
        rCstl = parse_krelease(count,1)
        vCstl = parse_krelease(count,0)
        if (rCstl == "mainline" and '-rc' not in vCstl) or (rCstl == "stable"):
            # If Mainline isn't a Release Candidate, consider it Stable
            if parse_krelease(1,1) == "stable" and parse_krelease(1,0) > vCstl:
                # Favor latest Stable if it's newer than a Non-RC Mainline
                vCstl = parse_krelease(1,0)
            return (vCstl)
        count = count + 1
    return (parse_krelease(0,0)) # if no 'stable' use latest


def kernel_name(version):
    """Obtain the Kernel Name for a given Kernel Version."""
    kmakefile = (
        'https://git.kernel.org/cgit/linux/kernel/git'
        '/stable/linux-stable.git/plain/Makefile?id=v' + version
        )

    for line in urllib.urlopen(kmakefile).read(128).splitlines():
        if "NAME" in line:
            return (line.split(' = ')[1])
