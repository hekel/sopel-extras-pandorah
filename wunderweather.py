# coding=utf8
"""
wunderweather.py - Wunderweather module
Copyright © 2016-2018, Adam Erdman https://pandorah.org
Licensed under the Eiffel Forum License 2.

This module requires a Wunderground API Key.
You can get a free developer key at: https://www.wunderground.com/weather/api
Make sure you add it to your sopel config like so:

[wunderground]
api_key = 0abcdef123456890
"""

from sopel.config.types import StaticSection, ValidatedAttribute
from sopel.module import commands, example, rate
import requests
from time import strftime, gmtime

class WunderSection(StaticSection):
    api_key = ValidatedAttribute('api_key')

def configure(config):
    config.define_section('wunderground', WunderSection)
    config.wunderground.configure_setting(
        'api_key',
        'Enter your Wunderground API Key\n'
        '(Get a free key here: https://www.wunderground.com/weather/api)'
    )

def setup(bot):
    bot.config.define_section('wunderground', WunderSection)
    global api_key
    global help_prefix
    api_key = bot.config.wunderground.api_key
    help_prefix = bot.config.core.help_prefix


@commands('wset', 'ws')
@example('!wset Hershey, PA')
@rate(3)
def weatherset(bot, trigger):
    """Set your default location for weather requests."""
    location = trigger.group(2)
    if not location:
        location = check_default_loc(bot, trigger)
        if location is False:
            return bot.say('You do not have a default location set.'
                ' \x1d(See: %shelp wset)\x1d' % help_prefix)
        return bot.say('Your default location is ' + location)

    check_location = query_api('conditions', location)
    response = check_location['response']

    if oopsie(bot, trigger, response, location) is False:
        bot.db.set_nick_value(trigger.nick, 'location', location)
        return bot.say('Your default location is now set as ' + location)


@commands('weather', 'w')
@example('!weather Hershey, PA \x1d(See also: help wset)\x1d')
@rate(3)
def wunderweather(bot, trigger):
    """Request weather information for a given location."""
    location = check_location(bot, trigger)
    if location is False:
        return bot.say('Please provide a location.'
            ' \x1d(See: %shelp weather)\x1d' % help_prefix)

    wcondition = query_api('conditions/forecast/astronomy', location)
    response = wcondition['response']

    if oopsie(bot, trigger, response, location) is False:
        return wundercondition(bot, trigger, location, wcondition)


@commands('forecast', 'wf', 'forecast1', 'wf1')
@example('!forecast Hershey, PA \x1d(See also: forecast3)\x1d')
@rate(4)
def wunderweather_forecast1(bot, trigger):
    """Request Today's Forecast for a given location."""
    return wunderforecast(bot, trigger, 'Today\'s')


@commands('forecast3', 'wf3')
@example('!forecast3 Hershey, PA \x1d(See also: forecast)\x1d')
@rate(4)
def wunderweather_forecast3(bot, trigger):
    """Request Three Day Forecast for a given location."""
    return wunderforecast(bot, trigger, 'Three Day')


def query_api(features, query):
    url = requests.get('http://api.wunderground.com/api/' +
            api_key + '/' + features + '/q/' + query + '.json')
    print('[Wunderground] Querying API: ' + query)
    return url.json()


def oopsie(bot, trigger, response, location):
    if 'error' in response:
        return error(bot, response)
    elif 'results' in response:
        return geolocations(bot, trigger, response, location)
    return False


def error(bot, response):
    error = response['error']
    if error['type'] == 'querynotfound':
        return bot.say(error['description'])
    return bot.say('[Wunderground] ' + error['type'] + ': ' +
                        error['description'])


def check_location(bot, trigger):
    try:
        location = bot.db.get_nick_value(trigger.group(2).strip(), 'location')
    except AttributeError:
        location = False
    else:
        if not location:
            location = trigger.group(2)
    if not location:
        location = check_default_loc(bot, trigger)
    return location

def check_default_loc(bot, trigger):
    location = bot.db.get_nick_value(trigger.nick, 'location')
    if not location:
        return False
    return location


def geolocations(bot, trigger, response, location):
    possible_locations = []
    for geo in response['results']:
        GeoCity = geo['name']
        CountryInitials = '\x1d(%s)\x1d' % geo['country']
        if geo['state'] != '':
            GeoState = geo['state'] + CountryInitials
        elif geo['country_name'] != '':
            GeoState = geo['country_name'] + CountryInitials
        else:
            GeoState = geo['country']
        possible_locations.append(GeoCity + ', ' + GeoState)
    return bot.say(trigger.nick + ', \"%s\" could be: ' % location +
        '; '.join(possible_locations[:-1]) + '; or ' + possible_locations[-1],
        max_messages=2)


def wundercondition(bot, trigger, location, wcondition):
    try:
        current = wcondition['current_observation']
        display_location = current['display_location']
        observe_location = current['observation_location']
    except KeyError:
        return bot.say(trigger.nick + 'There was a problem reaching the API.')

    highlow = ''
    if wcondition['forecast']: #maybe split me off for use in wunderforecast
        current_forecast = wcondition['forecast']
        forecast_today = current_forecast['simpleforecast']['forecastday'][0]
        high_f = str(forecast_today['high']['fahrenheit'])
        high_c = str(forecast_today['high']['celsius'])
        low_f = str(forecast_today['low']['fahrenheit'])
        low_c = str(forecast_today['low']['celsius'])
        highlow = u'\x02High/Low:\x02 %s/%s\u00B0F\x1d(%s/%s\u00B0C)\x1d' \
                % (high_f, low_f, high_c, low_c)

    if observe_location['state'] != '':
        location = observe_location['full']
    else:
        location = display_location['full']

    last_updated = ''
    if current['observation_epoch']:
        last_updated = '\x02Updated:\x02 %s UTC' \
                     % strftime('%b %d, %H:%M', \
                       gmtime(float(current['observation_epoch'])))

    station = ''
    if current['station_id']:
        station = '\x02Station:\x02 [%s]' % current['station_id']

    elevation = ''
    if observe_location['elevation'] != ' ft':
        elevation = '[%s]' % observe_location['elevation']

    weather = current['weather']
    location = '\x02%s\x02' % location

    temp_f = str(current['temp_f']) + u'\u00B0F'
    temp_c = str(current['temp_c']) + u'\u00B0C'
    temp = u' \U0001F321 %s\x1d(%s)\x1d' % (temp_f, temp_c)

    feelslike = ''
    feels_f = str(current['feelslike_f']) + u'\u00B0F'
    feels_c = str(current['feelslike_c']) + u'\u00B0C'
    if feels_f != temp_f:
        feelslike = '\x02Feels Like:\x02 %s\x1d(%s)\x1d' % (feels_f, feels_c)

    humidity = '\x02Humidity:\x02 ' + current['relative_humidity']

    wind_mph = '(%sMPH)' % str(current['wind_mph'])
    wind_kph = '(%sKPH)' % str(current['wind_kph'])
    wind_beaufort = beaufort(current['wind_mph'])
    wind_deg = '\x02%s\x02' % degree(current['wind_degrees'])
    wind_dir = current['wind_dir']
    wind_str = '\x02Wind:\x02 ' + ' '.join([wind_dir, wind_deg, wind_beaufort])

    dewpoint = ''
    dewpoint_f = str(current['dewpoint_f']) + u'\u00B0F'
    dewpoint_c = str(current['dewpoint_c']) + u'\u00B0C'
    if 'dewpoint_f' and 'dewpoint_c' in current:
        dewpoint = '\x02Dewpoint:\x02 %s\x1d(%s)\x1d' % (dewpoint_f, dewpoint_c)

    pressure = ''
    pressure_mb = current['pressure_mb'] + 'hPa/mbar'   #millibars
    pressure_in = current['pressure_in'] + 'inHg'       #inches of mercury
    pressure_td = current['pressure_trend'].replace('+', u' \u2191').replace('-', u' \u2193').replace('0', '')
    if 'pressure_mb' and 'pressure_in' in current:
        pressure = '\x02Pressure:%s\x02 %s\x1d(%s)\x1d' % (pressure_td, pressure_mb, pressure_in)

    sun_phase = ''
    if wcondition['response']['features']['astronomy']:
        moon_phase = '\x02Moon:\x02 %s' % \
            moon_icon(wcondition['moon_phase']['phaseofMoon'])
        sun_phase = '\x02Sun:\x02 \x1d%s:%s\x1d\x02-\x02\x1d%s:%s\x1d' % \
            (wcondition['sun_phase']['sunrise']['hour'],
            wcondition['sun_phase']['sunrise']['minute'],
            wcondition['sun_phase']['sunset']['hour'],
            wcondition['sun_phase']['sunset']['minute'])

    return bot.say(trigger.nick + ': ' + u' \u2016 '
            .join(filter(None, [location + u' \u2016' + temp, highlow, feelslike,
            dewpoint, humidity, weather_icon(weather), wind_str,
            pressure, sun_phase, moon_phase,
            station + elevation, last_updated])).strip()
        )


def wunderforecast(bot, trigger, forecast_type):
    location = check_location(bot, trigger)
    if location is False:
        return bot.say('Please provide a location.'
            ' \x1d(See: %shelp weather)\x1d' % help_prefix)
    wforecast = query_api('forecast/conditions', location)
    response = wforecast['response']

    if oopsie(bot, trigger, response, location) is False:
        full_location = wforecast['current_observation']['display_location']['full']
        forecastday = wforecast['forecast']['simpleforecast']['forecastday']
        forecastday_txt = wforecast['forecast']['txt_forecast']['forecastday']
        wunder_forecast = []
        for day_slice in forecastday_txt:
            day_title = day_slice['title']
            forecast_txt = day_slice['fcttext']
            fct_text = '\x02%s:\x02 \x1d%s\x1d' % (day_title, forecast_txt)
            wunder_forecast.append(fct_text)
        bot.say('%s Forecast for \x02%s\x02:' % (forecast_type, full_location))
        bot.say(u' \u2016 '.join(wunder_forecast[:2]))
        if forecast_type != 'Today\'s':
            bot.say(u' \u2016 '.join(wunder_forecast[2:4]))
            bot.say(u' \u2016 '.join(wunder_forecast[4:6]))
        return


def weather_icon(weather):
    weather_all = lambda m: all(ws in weather for ws in m)
    weather_any = lambda m: any(ws in weather for ws in m)

    icon = ''
    if weather == 'Clear':
        icon = u'\U0001F323'
    elif weather in ('Cloudy', 'Overcast', 'Scattered Clouds'):
        icon = u'\u2601'
    elif weather_any(['Mist', 'Haze', 'Partial Fog']):
        icon = u'\U0001F301'
    elif 'Fog' in weather:
        icon = u'\U0001F32B'
    elif weather in ('Mostly Cloudy', 'Partly Sunny'):
        icon = u'\U0001F325'
    elif weather in ('Partly Cloudy', 'Mostly Sunny'):
        icon = u'\U0001F324'
    elif weather == 'Scattered Showers':
        icon = u'\U0001F326'
    elif weather == 'Sunny':
        icon = u'\U0001F31E'
    elif 'Thunderstorms' in weather:
        icon = u'\u26C8'
    elif 'Thunderstorm' in weather:
        icon = u'\U0001F329'
    elif 'Snow Showers' in weather:
        icon = u'\U0001F328'
    elif weather_all(['Heavy', 'Snow']):
        icon = u'\u2746'
    elif weather_all(['Light', 'Snow']):
        icon = u'\u2745'
    elif weather_any(['Snow', 'Ice']):
        icon = u'\u2744'
    elif weather_any(['Drizzle', 'Sleet', 'Hail', 'Rain', 'Showers']):
        icon = u'\U0001F327'
    elif 'Sand' in weather:
        icon = u'\U0001F3DC'
    elif 'Volcanic Ash' in weather:
        icon = u'\U0001F30B'
    elif 'Funnel Cloud' in weather:
        icon = u'\U0001F32A'
    elif 'Unknown' in weather:
        icon = u'\u2753'

    if icon:
        icon = '\x02' + icon + '\x02 '
    return icon + weather


def moon_icon(phase):
    icon = ''
    if phase == 'New Moon':
        icon = u'\U0001F311'
    elif phase == 'Waxing Crescent':
        icon = u'\U0001F312'
    elif phase == 'First Quarter':
        icon = u'\U0001F313'
    elif phase == 'Waxing Gibbous':
        icon = u'\U0001F314'
    elif phase == 'Full':
        icon = u'\U0001F315'
    elif phase == 'Waning Gibbous':
        icon = u'\U0001F316'
    elif phase == 'Last Quarter':
        icon = u'\U0001F317'
    elif phase == 'Waning Crescent':
        icon = u'\U0001F318'

    if icon:
        icon = '\x02' + icon + '\x02 '
    return icon + phase


def beaufort(mph):
    beaufort = {
        1: 'Calm',
        4: 'Light air',
        7: 'Light breeze',
        11: 'Gentle breeze',
        18: 'Moderate breeze',
        24: 'Fresh breeze',
        31: 'Strong breeze',
        38: 'Near gale',
        46: 'Gale',
        54: 'Strong gale',
        63: 'Storm',
        73: 'Violent storm',
        9000: 'Hurricane'
    }
    return(beaufort[min(k for k in beaufort if k >= mph)])


def degree(deg):
    degrees = {
        22.5: u'\u2193',
        67.5: u'\u2199',
        112.5: u'\u2190',
        157.5: u'\u2196',
        202.5: u'\u2191',
        247.5: u'\u2197',
        292.5: u'\u2192',
        337.5: u'\u2198',
        9000: u'\u2193'
    }
    return(degrees[min(k for k in degrees if k >= deg)])
