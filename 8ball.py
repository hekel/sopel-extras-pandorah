# coding=utf8
"""
8ball.py - Ask the foul mouthed magic 8ball a question

Copyright 2016-2018, Adam Erdman https://pandorah.org
Copyright 2013, Sander Brand http://brantje.com
Licensed under the Eiffel Forum License 2.

http://sopel.dfbta.net
Additional responses added for #pandorah
"""

import sopel
import random
@sopel.module.commands('8', '8ball')
def ball(bot, trigger):
    """Ask the magic 8ball a question! Usage: !8 <question>"""
    messages = ["It is certain.",
                "It is decidedly so.",
                "Without a doubt.",
                "Yes, definitely.",
                "You may rely on it.",
                "As I see it, yes.",
                "Most likely.",
                "Outlook good.",
                "Yes.",
                "Signs point to yes.",
                "Reply hazy try again.",
                "Ask again later.",
                "Better not tell you now.",
                "Cannot predict now.",
                "Concentrate and ask again.",
                "Don't count on it.",
                "My reply is no.",
                "My sources say no.",
                "Outlook not so good.",
                "Very doubtful.",
                "God says no.",
                "Definitely not.",
                "What made you think I would know?",
                "Fo Sho.",
                "Fuck No.",
                "Fuck Yes.",
                "It's a possibility.",
                "Not possible.",
                "Nah.",
                "Yeah, lol.",
                "Stop bothering me.",
                "Could you ask again when I don't have bitches over?",
                "My sources say no. They also tell me they hate you and hope you burn in hell.",
                "All signs point to yes. On second thought, go fuck yourself.",
                "Stahp asking me fucking questions. Fuck!",
                "Bitch, Please.",
                "Probably.",
                "Probably not.",
                "Does the Pope shit in the woods?",
                "Do bears shit in the woods?",
                "Do politicians kill high class prostitutes?",
                u"Do I have to answer that right now?",
                "You're fucking kidding, right?",
                "I'm not even gonna fucking answer that.",
                "No fuckin way.",
                "Nope.",
                "I don't think so.",
                "Not that I'm aware.",
                "Of course.",
                "Of course not.",
                u"I don't know, I'm not into Pok\u00E9mon.",
                "If you think it's true, that's all that matters kiddo.",
                u"\u2026",
                u"¯\_(ツ)_/¯",
                "Could be...",
                "Perhaps.",
                "I guess!",
                "I'm not fucking answering that.",
                "What do you think?",
                "I think so.",
                "I think so... maybe?",
                "Sure.",
                "Absolutely.",
                "Absolutely not.",
                "Positively.",
                "Positively not.",
                "Negative.",
                "Nay.",
                "Never.",
                "Not at all.",
                "Yeah, nah, yeah.",
                "Yeah nah.",
                "Idk dude, maybe.",
                "Dunno, probably.",
                "Let me think about it first",
                "Can I think about it?",
                "Could be, IDK!",
                u"What do \u0002\u001Dyou\u0002\u001D want me to say?",
                "If I say yes, will you leave me alone?",
                "If I say no, will you leave me alone?",
                "Nobody ever just asks me how I'm doing."]

    answer = random.randint(0,len(messages) - 1)
    bot.say(messages[answer]);
