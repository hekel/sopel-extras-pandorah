# coding=utf8
"""
checktest.py - Check/Test the validity of a statement
Copyright © 2016, Adam Erdman https://pandorah.org
Licensed under the Eiffel Forum License 2.

Inspired by the test/check factoid/alias on the Limnoria/Supybot "phrik" found
in #archlinux channels on Freenode.net
"""

from sopel.module import commands, example
from sopel.formatting import color, colors
from random import choice


response = ['PANIC', ' OK ',
            'PASS', 'PASS', 'PASS', 'PASS', 'PASS', 'PASS', 'PASS', 'PASS',
            'FAIL', 'FAIL', 'FAIL', 'FAIL', 'FAIL', 'FAIL', 'FAIL', 'FAIL']


@commands('check')
@example('!check if pigs can fly')
def check(bot, trigger):
    """ Check/Test the validity of a statement """
    if not trigger.group(2):
        bot.say(checktest())
    else:
        bot.say('\x0fTesting ' + trigger.group(2).strip() + ': ' + checktest())


@commands('test')
def test(bot, trigger):
    """ Returns [\x0303PASS\x0f] or [\x0304FAIL\x0f] """
    bot.say(checktest())


def checktest():
    result = choice(response)
    if result in ['PASS', ' OK ']:
        result = color(result, colors.GREEN)
    else:
        result = color(result, colors.RED)
    return ('[' + result + ']')
