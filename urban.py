"""
urban.py - Urban Dictionary module

This module provides search method for urban dictionary

This module has been ported for use with sopel 6.3.0 from the willie module:
https://github.com/cerealcable/cereal-willie-extras/blob/master/urban.py
"""

from sopel.module import commands, rule, priority
from sopel import tools
import requests
import re

urban_api = "http://api.urbandictionary.com/v0/define?term={}"
urban_random_api = "http://api.urbandictionary.com/v0/random"
regex = re.compile("^(?P<query>.*?)(\s+(?P<index>\d+))?$")
format_codes = {'right_arrow': u'\u21d2', 'f_clear': u'\x0f', 'f_bold': u'\x02', 'f_red': u'\x0304', 'f_green': u'\x0303', 'f_black': u'\x0301', 'f_blue': u'\x0302', 'f_brown': u'\x0305', 'f_purple': u'\x0306', 'f_orange': u'\x0307', 'f_yellow': u'\x0308', 'f_light_green': u'\x0309', 'f_teal': u'\x0310', 'f_cyan': u'\x0311', 'f_light_blue': u'\x0312', 'f_pink': u'\x0313', 'f_grey': u'\x0314', 'f_light_grey': u'\x0315'}

@commands('urban', 'ud')
@priority('low')
def urban(bot, trigger):
    q = trigger.group(2)
    if not q:
        random(bot)
        # bot.say('Usage: !{} [search term] <index>'.format(trigger.group(1)))
    else:
        r = regex.search(q)
        rdict = r.groupdict()
        query = rdict['query']
        index = rdict.get('index', None) or 1
        index = int(index)
        define(bot, query, index)

def define(bot, query, index):
    p_index = index
    index = index - 1

    page = requests.get(urban_api.format(query))
    data = page.json()
    if not data['list']:
        bot.say("Couldn't find {} )-:".format(query))
    elif data['list'][0]['definition']:
        if p_index > len(data['list']) or p_index <= 0:
            bot.say("HERP DERP!!!  You can't use an index like that, RTFM")
        else:
            # definition
            bot.say(u"{f_bold}{word} {index}/{count}{f_clear} {f_green}+{up_votes}{f_clear} {f_red}-{down_votes}{f_clear} {definition}".format(word=query, index=p_index, count=len(data['list']), up_votes=data['list'][index]['thumbs_up'], down_votes=data['list'][index]['thumbs_down'], definition=data['list'][index]['definition'], **format_codes), max_messages=2)
            # example
            bot.say(u"  {f_bold}{right_arrow} Example{f_clear} {example}".format(example=data['list'][index]['example'], **format_codes))
    else:
        bot.say("Something went wrong...")
        print("Something went wrong...")
        print(data)


def random(bot):
    page = requests.get(urban_random_api)
    data = page.json()
    if data.get('list', None):
        item = None
        score = None
        for i in data['list']:
            i_score = int(i['thumbs_up']) - int(i['thumbs_down'])
            if score:
                if i_score > score:
                    score = i_score
                    item = i
            else:
                score = i_score
                item = i

        # definition
        bot.say(u"{f_bold}{word}{f_clear} {f_green}+{up_votes}{f_clear} {f_red}-{down_votes}{f_clear} {definition}".format(word=item['word'], up_votes=item['thumbs_up'], down_votes=item['thumbs_down'], definition=item['definition'], **format_codes), max_messages=2)
        # example
        bot.say(u"  {f_bold}{right_arrow} Example{f_clear} {example}".format(example=item['example'], **format_codes))
    else:
        bot.say("Something went wrong...")
        print("Something went wrong...")
        print(data)
