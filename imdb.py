# coding=utf-8
"""
imdb.py - Sopel Movie Information Module
Copyright © 2012-2013, Elad Alfassa, <elad@fedoraproject.org>
Copyright © 2017, Adam Erdman https://pandorah.org
Licensed under the Eiffel Forum License 2.

This module relies on omdbapi.com and now requires an API key
You can obtain a free API key here https://www.omdbapi.com/apikey.aspx
Make sure you add it to your sopel config like so:

[imdb]
api_key = a0b1c2d3
"""
from __future__ import unicode_literals, absolute_import, print_function, division
import requests
from sopel.config.types import StaticSection, ValidatedAttribute
import sopel.module
from sopel.logger import get_logger
from sopel.formatting import color, colors

class imdbSection(StaticSection):
    api_key = ValidatedAttribute('api_key')

def configure(config):
    config.define_section('imdb', imdbSection)
    config.imdb.configure_setting(
        'api_key',
        'Enter your OMDb API Key\n'
        '(Get a free key here: https://www.omdbapi.com/apikey.aspx)'
    )

def setup(bot):
    bot.config.define_section('imdb', imdbSection)
    global api_key
    api_key = bot.config.imdb.api_key

LOGGER = get_logger(__name__)

colorlogo = (color('\x02[IMDb]\x02', colors.BLACK, colors.YELLOW))

@sopel.module.commands('imdb', 'movie')
@sopel.module.example('.!imdb ThisTitleDoesNotExist', colorlogo + ' Title not found!')
@sopel.module.example('!imdb Requiem for a Dream', colorlogo + ' Title: Requiem for a Dream | Year: 2000 | Rating: 8.4 | Genre: Drama | Link: http://imdb.com/title/tt0180093')
def movie(bot, trigger):
    """
    Returns some information about a movie, like Title, Year, Rating, Genre and IMDB Link.
    """
    if not trigger.group(2):
        return
    word = trigger.group(2).rstrip()
    uri = "http://www.omdbapi.com/?apikey=%s&" % api_key
    data = requests.get(uri, params={'t': word}, timeout=30,
                        verify=bot.config.core.verify_ssl).json()
    if data['Response'] == 'False':
        if 'Error' in data:
            message = colorlogo + ' %s' % data['Error']
        else:
            LOGGER.warning(
                'Got an error from the OMDb api, search phrase was %s; data was %s',
                word, str(data))
            message = colorlogo + ' Got an error from OMDbapi'
    else:
        message = colorlogo + ' Title: ' + data['Title'] + \
                  ' | Year: ' + data['Year'] + \
                  ' | Rating: ' + data['imdbRating'] + \
                  ' | Genre: ' + data['Genre'] + \
                  ' | Link: http://imdb.com/title/' + data['imdbID']
    bot.say(message)


if __name__ == "__main__":
    from sopel.test_tools import run_example_tests
    run_example_tests(__file__)
