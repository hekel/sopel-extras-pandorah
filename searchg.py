# coding=utf8
"""
searchg.py - Google Search module
Copyright © 2016, Adam Erdman https://pandorah.org
Licensed under the Eiffel Forum License 2.

This module requires a Google API Key.
You can get a free developer key at:
https://developers.google.com/api-client-library/python/guide/aaa_apikeys
You also need a Google Custom Search Engine ID.
https://support.google.com/customsearch/answer/2649143
Make sure you add them to your sopel config like so:

[google]
api_key = Ex4MPle5eARch_G00g13AP1KeyF0RsearchgMOD
cse_id = 012345678910111213141:abcdefghijk

NOTE: This module's .g command will conflict with Sopel's official search.py
      because it's "g" has been aliased to it's duck search function.
      (https://github.com/sopel-irc/sopel/commit/ef57bbd)
      (https://github.com/sopel-irc/sopel/issues/650)
      This can be worked around by removing the "g" alias in the official module
"""

from sopel.module import commands, example
from sopel.config.types import StaticSection, ValidatedAttribute, NO_DEFAULT
from sopel import tools
from googleapiclient.discovery import build

color_logo = "\x0F\x02[\x0302G\x0304o\x0308o\x0302g\x0303l\x0304e\x0F\x02]\x02"

class GoogleSection(StaticSection):
    api_key = ValidatedAttribute('api_key', default=NO_DEFAULT)
    """The Google API key to auth to the endpoint"""
    cse_id = ValidatedAttribute('cse_id', default=NO_DEFAULT)
    """The Google CustomSearchEngine ID to auth to the endpoint"""

def configure(config):
    config.define_section('google', GoogleSection, validate=False)
    config.google.configure_setting(
        'api_key',
        'Enter your Google API key.',
    )
    config.google.configure_setting(
        'cse_id',
        'Enter your Google CustomSearch ID.',
    )

def setup(bot):
    bot.config.define_section('google', GoogleSection)
    if not bot.memory.contains('url_callbacks'):
        bot.memory['url_callbacks'] = tools.SopelMemory()
    global API
    API = build("customsearch", "v1", developerKey=bot.config.google.api_key)

def shutdown(bot):
    del bot.memory['url_callbacks'][regex]


@commands('g', 'google')
@example('!g vivaldi browser')
def google_search(bot, trigger):
    """Queries Google for the specified input."""
    if not trigger.group(2):
        return
    results = API.cse().list(
        q=trigger.group(2),
        cx=bot.config.google.cse_id,
        num=1
        ).execute()
    results = results.get('items')
    if not results:
        bot.reply("\x0FI couldn't find any %s results matching your query." % \
        color_logo)
        return

    bot.say("%s %s - \x1D\x02%s\x0F" % \
        (color_logo,
        results[0]['title'],
        results[0]['link'])
        )
